require("./bootstrap");

window.Vue = require("vue");

import InfiniteLoading from "vue-infinite-loading";
Vue.use(InfiniteLoading, {
    /* options */
});

//full page loader
import loading from "vue-full-loading";
Vue.component("loading", loading);

//emoji
import EmojiPicker from "@zaichaopan/emoji-picker";
Vue.use(EmojiPicker);

// import socketio from 'socket.io-client'
// window.Vue = require('vue');
// Vue.use(VueSocketio, socketio(':8000'));

Vue.component("Louncher", require("./components/Louncher.vue"));

const app = new Vue({
    el: "#app",
    data: {
        selected: 0,
        userId: 0,
        activeUser: 0,
        activeUserDetail: "",
        opositeActiveUser: 0,
        opositeUserSelectedContact: 0,
        allOnlineUsers: null,
        allMszs: null,
        allContacts: null,
        spinning: false,
        users: true,
        activeUsersIcon: true,
        searchIcon: true,
        usersIcon: false
    }
});
