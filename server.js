var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(8890);
let connectedUsers = []

function addUser(userList, user){
	let newList = Object.assign({}, userList)
	newList[user.name] = user
	return newList
}
function removeUser(userList, username){
	let newList = Object.assign({}, userList)
	delete newList[username]
	return newList
}




io.on('connection', (socket) => {


    socket.on('new_user_connected', (user)=>{
		user.socketId = socket.id
		connectedUsers = addUser(connectedUsers, user)
		socket.user = user

		io.emit('new_user_connected', connectedUsers)

    })

    socket.on('disconnect', ()=>{
		if("user" in socket){
			connectedUsers = removeUser(connectedUsers, socket.user.name)

			io.emit('user_disconnect', connectedUsers)
		}
	})
    
    socket.on('chat-message', (msg) => {
        socket.broadcast.emit('chat_message', msg)
    })

    socket.on('active_user', (data) => {
        socket.broadcast.emit('opposite_active_user', data)
    })
    socket.on('selected_contact', (data) => {
        socket.broadcast.emit('opposite_user_selected_contact', data)
    })

    socket.on('typing', (data) => {
        socket.broadcast.emit('typing', data)
    })
    socket.on('stoptyping', (data) => {
        socket.broadcast.emit('stoptyping', data)
    })

    socket.on('msg_seen', (userId) => {
        socket.broadcast.emit('msg_seen', userId)
    })
    socket.on('user_is_active', (userId) => {
        socket.broadcast.emit('user_is_active', userId)
    })


});